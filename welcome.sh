    #!/bin/bash
    ###################
    # Basé sur https://debian-facile.org/git/ProjetsDF/dfiso-buster/src/branch/master/config/includes.chroot/usr/share/dflinux/welcome.sh
    # Sur une proposition de Benoît 

    # message d'accueil
    zenity --title "Bienvenue" --text-info --checkbox="Cocher pour ne pas revoir ce message au prochain démarrage" \
        --cancel-label="Continuer et revoir ce message au prochain démarrage" --ok-label "Bonne découverte et @+" \
        --filename="/usr/share/linuxmaine/welcome.html" --html --width=800 --height=600

    # déplacement du lanceur de l'écran d'accueil sur le bureau
    
    case $? in
    0)
        if [ -e /home/$USER/.config/autostart/welcome.desktop ]; then
          mv /home/$USER/.config/autostart/welcome.desktop /home/$USER/Bureau
        fi
        ;;
    esac
    exit 0
