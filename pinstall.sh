#!/bin/bash

# vérification sudo
if [ $EUID -ne 0 ]
then
        echo "Vous devez lancer ce script avec des privilèges root (sudo bash pinstall.sh)"
        exit 1
else
        echo "Script exécuté avec les privilèges root"
fi

# le proxy est utilisé uniquement au local linuxmaine
# echo "Acquire::http::Proxy \"http://192.168.1.99:3142\";" >  /etc/apt/apt.conf.d/proxy

# mise à jour 
apt -y update
apt -y upgrade

# désinstallation de abiword et gnumeric
apt -y purge abiword
apt -y purge gnumeric

# installation de synaptic
apt install -y synaptic

# installation de impress
apt install -y libreoffice-impress

# installation du lecteur multimedia
apt-get -y install vlc

# installation du logiciel d'inventaire
export DEBIAN_FRONTEND=noninteractive
apt-get -q -y install ocsinventory-agent
echo "server=local.linuxmaine.org/ocsinventory" > /etc/ocsinventory/ocsinventory-agent.cfg
ocsinventory-agent

# installation et configuration de libdvd-pkg
apt install -y libdvd-pkg
dpkg-reconfigure libdvd-pkg
# nettoyage 
apt-get autoremove

# 
#rm  /etc/apt/apt.conf.d/proxy
#rm /etc/apt/apt.conf

#cat /etc/network/interfaces

#raccourcis libreoffice sur le bureau
echo -e  "[Desktop Entry]\nTerminal=false\nIcon=libreoffice-calc\nType=Application\nExec=libreoffice --calc\nName[fr]=LibreOffice Calc" > ~/Bureau/libreoffice-calc.desktop 
echo -e  "[Desktop Entry]\nTerminal=false\nIcon=libreoffice-writer\nType=Application\nExec=libreoffice --writer\nName[fr]=LibreOffice Writer" > ~/Bureau/libreoffice-writer.desktop 
chmod 755 ~/Bureau/libreoffice-calc.desktop
chmod 755 ~/Bureau/libreoffice-writer.desktop
if ! [ -e ~/.config/autostart/welcome.desktop ]; then
	mkdir -p ~/.config/autostart
        wget -O ~/.config/autostart/welcome.desktop "https://framagit.org/linuxmaine/unattended/-/raw/master/welcome.desktop"
        chmod 755 ~/.config/autostart/welcome.desktop
	chown -R linuxmaine:linuxmaine ~/.config/autostart
fi	

if ! [ -e /usr/share/linuxmaine/welcome.sh]; then
	mkdir -p /usr/share/linuxmaine
        wget -O /usr/share/linuxmaine/welcome.sh "https://framagit.org/linuxmaine/unattended/-/raw/master/welcome.sh"
        chmod 755 /usr/share/linuxmaine/welcome.sh
fi

if ! [ -e /usr/share/linuxmaine/welcome.html ]; then
       wget -O /usr/share/linuxmaine/welcome.html "https://framagit.org/linuxmaine/unattended/-/raw/master/welcome.html"
fi

#copier le lanceur de firefox sur le bureau
cp /usr/share/applications/firefox.desktop ~/Bureau
chmod 755 ~/Bureau/firefox.desktop

# Icônes du bureau en taille 38, 2 bureaux virtuels, ajout de l'appelt switcher de bureau et changement du fond d'écran
su $SUDO_USER -m -c 'xfconf-query -c xfce4-desktop -p /desktop-icons/icon-size -s 38
                     xfconf-query  -c xfwm4 -p /general/workspace_count -s 2
		     xfce4-panel --add pager
		     xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/last-image -s "/usr/share/xfce4/backdrops/leonardo_cardozo_ferreira__peachy__4k.jpg"
		     '
